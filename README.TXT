OpenMajeranek
================

OpenMajeranek is a browser-based game written using Java Server Pages, 
inspired by Club Peunguin and made by three people who've never played 
Club Peunguin and don't know Java.


Objectives
================

The game will allow you to create a custom character, you have the 
option to play either as a Carrot or a Potato. 

Your character may then participate in various activities located 
around the map - play minigames and earn in-game currency which then 
can be spent to purchase and customize areas of the map or buy swag 
for your character.

There will be a chat system which you can use to communicate with 
other players. You can also form groups and make friends using the 
in-game friend system.


Repository
================

In case this repository is mirrored to gitlab, please take in mind 
that all issues and feature requests submitted there might be ignored. 
If you'd like to report an issue or request a feature, please do so 
at the official Fossil repository located at:

https://fossil.figdor32.pl/OpenMajeranek/


Deployment
================

We test and deploy the project using Apache Tomcat 9. Since everything 
is written in Java, you should be able to deploy the game to mostly any 
OS you want. Instructions for deployment on FreeBSD will be published once 
the project is mature enough to be production-tested. You'll probably only 
need Tomcat, a webserver and PostgreSQL to get up and running. 

A public instance will be hosted someday.


Name
================

The name comes from a Polish word for Marjoram (Origanum majorana). 
It has absolutely nothing to do with the project.


License
================

Licensed under the Public Domain-Equivalent 0-Clause BSD. See COPYING for full text.
